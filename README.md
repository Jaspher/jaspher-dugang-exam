# youtube-project

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

Exam by [Jaspher T. Dugang], 
[March 21, 2021], [10:00 AM], [05:00 PM]
[March 22, 2021], [05:00 PM], [11:00 PM]

The setup of the project is PHP for the backend and VueJS for the frontend which needs to be hosted in two different domains.

A user had to input the youtube channel name and select to the list of youtube channels to fetch the latest 100 videos. 
