<?php

/**
 * Youtube API constant
 */
class ApiConstant
{
    const API_KEY = 'AIzaSyB5r_xUjsd8QSqU0GBiitmkguuf-_gDE8E';

    const SEARCH_CHANNEL_API = 'https://youtube.googleapis.com/youtube/v3/search?part=snippet&channelType=any&maxResults=3&type=channel&key=' . SELF::API_KEY;

    const SEARCH_FIELD = '&q=';

    const FETCH_VIDEO_BY_CHANNEL_ID_API = 'https://youtube.googleapis.com/youtube/v3/search?part=snippet&maxResults=50&order=date&type=video&key=' . SELF::API_KEY;

    const CHANNEL_ID_FIELD = '&channelId=';

    const PAGE_TOKEN = '&pageToken=';
}
?>
