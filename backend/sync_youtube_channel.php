<?php
header("Access-Control-Allow-Origin: http://localhost:8080");
header("Access-Control-Allow-Headers: *");
include('./api/YoutubeApi.php');
require_once('./database/DatabaseConnection.php');

/**
 * Validate Parameters for request
 */
if (isset($_POST['method'])) {
    switch ($_POST['method']) {
        case 'searchChannel':
            $oResponse = searchChannel($_POST['channel_name']);            
            echo json_encode($oResponse);
          break;
        case 'fetchVideoChannel':
            $oResponse = fetchVideoChannel($_POST);
            echo json_encode($oResponse);
        break;
        default:
          echo json_encode(['err' => 'something went wrong']);
      }
}

/**
 * Search Youtube Channel
 */
function searchChannel($sChannel)
{
    $aData = ['channel' => $sChannel];
    $oYoutubeApi = new YoutubeApi($aData);
    return $oYoutubeApi->fetchAllChannelByName();
}

/**
 * Fetch Videos of Youtube Channel
 */
function fetchVideoChannel($aParams)
{
    $aData = [
        'channel_id' => $aParams['channel_id']
    ];
    
    $iId = saveChannelInfoToDB($aParams);
    saveChannelInfoToJson($aParams);
    $oYoutubeApi = new YoutubeApi($aData);    
    $aResponse = $oYoutubeApi->fetchVideoByChannelId();
    saveVideosToDB($aResponse, $iId);
    return readYoutubeChannelJson();
}

/**
 * Read Youtube Channel JSON
 */
function readYoutubeChannelJson()
{
    $aJsonFile = file_get_contents('youtube_channel_json.php');
    $aDecodedJson = json_decode($aJsonFile, true);
    return $aDecodedJson;
}

/**
 * Save youtube channel information to database
 */
function saveChannelInfoToDB($aData)
{    
    $oDatabase = new DatabaseConnection();
    return $oDatabase->saveChannelInformation($aData);
}

/**
 * Save videos of youtube channel to database
 */
function saveVideosToDB($aData, $iId)
{
    
    if (empty($aData['items']) || !isset($aData['items'])) {
        return;
    }
    foreach ($aData['items'] as $key => $value) {
        $sVideoLink = 'https://www.youtube.com/watch?v=' . $aData['items'][$key]['id']['videoId'];
        $sTitle =  $aData['items'][$key]['snippet']['title'];
        $sDescription =  $aData['items'][$key]['snippet']['description'];
        $sThumbnail =  $aData['items'][$key]['snippet']['thumbnails']['high']['url'];
        $aParams = [
            'video_link'  => $sVideoLink,
            'title'       => $sTitle,
            'description' => $sDescription,
            'thumbnail'   => $sThumbnail,
            'channel_id'  => $iId
        ];
        saveVideosToJson($aParams);
        $oDatabase = new DatabaseConnection();
        $oDatabase->saveVideos($aParams);
    }    
}

/**
 * Save youtube channel information to youtube_channel_json.php
 */
function saveChannelInfoToJson($aData)
{   
    $aBuildData = [
        'channel_id' => $aData['channel_id'],
        'profile_picture' => $aData['profile_picture'],
        'description' => str_replace("'", "\'", $aData['description']),
        'name' => $aData['name']
    ];
    file_put_contents('youtube_channel_json.php', json_encode($aBuildData));
}

/**
 * Save videos of youtube channel information to youtube_channel_json.php
 */
function saveVideosToJson($aParams)
{
    $aJsonFile = file_get_contents('youtube_channel_json.php');
    $aDecoded = json_decode($aJsonFile, true);
    
    if (isset($aDecoded['videos'])) {
        array_push($aDecoded['videos'], $aParams);
    } else {
        $aDecoded['videos'] = [$aParams];
    } 
    file_put_contents('youtube_channel_json.php', '');   
    file_put_contents('youtube_channel_json.php', json_encode($aDecoded));
}
