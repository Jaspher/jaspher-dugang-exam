<?php

class DatabaseConnection
{
  /**
   * Instance of Mysqli
   */
  public $oMysqli;

  /**
   * Inserted Id
   */
  public $iInserId;

  /**
   * DatabaseConnection Contructor
   */
  public function __construct()
  {
    $this->setupConnection();
  }
  
  /**
   * Setup connection for MySQL
   */
  public function setupConnection()
  {
    $this->oMysqli = new mysqli('localhost', 'root', '', 'youtube_db');
    if ($this->oMysqli->connect_errno) {
      echo "Failed to connect to MySQL: " . $this->oMysqli->connect_error;
      exit();
    }
    $this->createYoutubeChannelTable();
    $this->createYoutubeChannelVideosTable();
  }

  /**
   * Create youtube channel table
   */
  private function createYoutubeChannelTable()
  {
    $sYoutubeChannelTable = "CREATE TABLE IF NOT EXISTS youtube_channels (
      id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
      profile_picture TEXT NOT NULL,
      name TEXT NOT NULL,
      description TEXT NOT NULL
      )";
      $this->oMysqli->query($sYoutubeChannelTable);
  }


  /**
   * create youtube channel videos table
   */
  private function createYoutubeChannelVideosTable()
  {
    $sYoutubeChannelVideosTable = "CREATE TABLE IF NOT EXISTS youtube_channel_videos (
      id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
      video_link TEXT NOT NULL,
      title TEXT NOT NULL,
      description TEXT NOT NULL,
      thumbnail TEXT NOT NULL,
      channel_id INT(11) UNSIGNED NOT NULL,
      FOREIGN KEY (channel_id) REFERENCES youtube_channels(id)
      )";
    $this->oMysqli->query($sYoutubeChannelVideosTable);
  }

  /**
   * Save channel information to database
   */
  public function saveChannelInformation($aData)
  {
    $sProfilePicture = $aData['profile_picture'];
    $sName = $aData['name'];
    $sDescription = str_replace("'", "\'", $aData['description']);
    $sQuery = "INSERT INTO youtube_channels (profile_picture, name, description)
    VALUES ('$sProfilePicture', '$sName', '$sDescription')";
    $this->oMysqli->query($sQuery);
    return $this->oMysqli->insert_id;
  }

  /**
   * Save videos of selected youtube channel to database
   */
  public function saveVideos($aData)
  {
    $sVideoLink = $aData['video_link'];
    $sTitle = str_replace("'", "\'", $aData['title']);
    $sThumbnail = $aData['thumbnail'];
    $sId = $aData['channel_id'];
    $sDescription = str_replace("'", "\'", $aData['description']);
    $sQuery = "INSERT INTO youtube_channel_videos (video_link, title, thumbnail, description, channel_id)
    VALUES ('$sVideoLink', '$sTitle', '$sThumbnail', '$sDescription', '$sId')";
    $this->oMysqli->query($sQuery);
  }
}




