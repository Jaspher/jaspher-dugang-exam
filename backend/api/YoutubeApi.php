<?php
require_once('./Constant/ApiConstant.php');

class YoutubeApi
{

    /**
     * Params
     */
    private $aParams;

    /**
     * YoutubeAPI Constructor
     */
    public function __construct($aParams)
    {
        $this->aParams = $aParams;
    }

    /**
     * Fetch all channel by youtube channel name
     */
    public function fetchAllChannelByName()
    {
        $sUrl = ApiConstant::SEARCH_CHANNEL_API . ApiConstant::SEARCH_FIELD . $this->aParams['channel'];       
        return $this->curlGetRequest($sUrl);
    }

    /**
     * Fetch video by by channel id
     */
    public function fetchVideoByChannelId()
    {
        $sUrl = ApiConstant::FETCH_VIDEO_BY_CHANNEL_ID_API . ApiConstant::CHANNEL_ID_FIELD . $this->aParams['channel_id'];
        $aFirstResponse = $this->curlGetRequest($sUrl);
        if (isset($aFirstResponse['nextPageToken'])) {
            $sNextUrl = $sUrl . ApiConstant::PAGE_TOKEN . $aFirstResponse['nextPageToken'];
            $aSecondResponse = $this->curlGetRequest($sNextUrl);
            $aMergeItems = array_merge($aSecondResponse['items'], $aFirstResponse['items']);
            $aSecondResponse['items'] = $aMergeItems;
            return $aSecondResponse;
        }
        return $aFirstResponse;
        
    }

    /**
     * Get curl request api
     */
    private function curlGetRequest($sUrl)
    {
        $oCh = curl_init();
        curl_setopt($oCh, CURLOPT_URL, $sUrl);
        curl_setopt($oCh, CURLOPT_RETURNTRANSFER, true);

        $oResponse = curl_exec($oCh);
        return json_decode($oResponse, true);
    }

}